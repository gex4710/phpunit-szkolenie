<?php

declare(strict_types=1);

namespace App\PrivateProtected;

class BlackInk implements InkInterface
{
    public const COLOR = 'BLACK';
    /**
     * @var bool
     */
    private $isFull;

    public function __construct(bool $isFull)
    {
        $this->isFull = $isFull;
    }

    public function getInk(): ?string
    {
        return $this->isEmpty() ? null : self::COLOR;
    }

    public function isEmpty(): bool
    {
        echo "Checking...";
        sleep(3);
        return !$this->isFull;
    }
}