<?php

declare(strict_types=1);

namespace App\PrivateProtected;

class ColorInk implements InkInterface
{
    public const COLOR = 'COLOR';
    /**
     * @var bool
     */
    private $isFull;

    public function __construct(bool $isFull)
    {
        $this->isFull = $isFull;
    }

    public function getInk(): ?string
    {
        return $this->isEmpty() ? null : self::COLOR;
    }

    public function isEmpty(): bool
    {
        return !$this->isFull;
    }
}