<?php

namespace App\PrivateProtected;


interface InkInterface
{
    /**
     * @return string|null
     */
    public function getInk(): ?string;

    /**
     * @return bool
     */
    public function isEmpty(): bool;
}