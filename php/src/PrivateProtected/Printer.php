<?php

declare(strict_types=1);

namespace App\PrivateProtected;

class Printer extends MultiFunctionDevice
{
    /**
     * @var BlackInk
     */
    private $blackInk;

    /**
     * @var ColorInk
     */
    private $colorInk;

    /**
     * Printer constructor.
     * @param BlackInk $blackInk
     * @param ColorInk $colorInk
     */
    public function __construct(BlackInk $blackInk, ColorInk $colorInk)
    {
        $this->blackInk = $blackInk;
        $this->colorInk = $colorInk;
    }

    /**
     * @param string $text
     * @param string $color
     * @return Document|null
     */
    public function print(string $text, string $color = BlackInk::COLOR): ?Document
    {
        if (!$this->checkInk($color) && $this->isDeviceReady()) {
            $ink = $this->getCorrectInk($color);
            return new Document($text, $ink);
        }

        return null;
    }

    protected function checkInk(string $color): bool
    {
        switch ($color) {
            case BlackInk::COLOR:
                return $this->blackInk->isEmpty();
                break;
            case ColorInk::COLOR:
                return $this->colorInk->isEmpty();
                break;
        }
    }

    private function getCorrectInk(string $color): InkInterface
    {
        switch ($color) {
            case BlackInk::COLOR:
                return $this->blackInk;
                break;
            case ColorInk::COLOR:
                return $this->colorInk;
                break;
        }
    }
}