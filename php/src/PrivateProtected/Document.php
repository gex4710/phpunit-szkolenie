<?php

declare(strict_types=1);

namespace App\PrivateProtected;

class Document
{
    /**
     * @var string
     */
    private $text;
    /**
     * @var InkInterface
     */
    private $ink;

    public function __construct(string $text, InkInterface $ink)
    {
        $this->text = $text;
        $this->ink = $ink;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getColor(): ?string
    {
        return $this->ink->getInk();
    }
}