<?php

namespace App\Mock;

class User
{
    public $first_name;
    public $surname;
    public $email;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(?Mailer $mailer = null, ?Logger $logger = null)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function getFullName()
    {
        return \trim("$this->first_name $this->surname");
    }

    public function notify($message)
    {
        $this->logger->log(\sprintf('Mail sent, message: %s | receiver: %s', $message, $this->email));
        return $this->mailer->sendMessage($this->email, $message);
    }
}
