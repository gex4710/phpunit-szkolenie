<?php

namespace App\Mock;

class Logger
{
    public function log(string $log): bool
    {
        \sleep(3);

        echo 'Logged: ' . $log;

        return true;
    }
}