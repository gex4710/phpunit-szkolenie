<?php

namespace App\DataProvider;

class Article
{
    private $text;

    public function addArticle(string $text): void
    {
        $this->text = \strip_tags($text);
    }

    public function getArticle(): ?string
    {
        return $this->text;
    }
}