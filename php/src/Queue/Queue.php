<?php

namespace App\Queue;

class Queue
{
    public const MAX_QUEUE_LENGTH = 5;

    protected $items = [];

    public function push($item): void
    {
        if ($this->getCount() === self::MAX_QUEUE_LENGTH) {
            throw new TooManyItemsInQueue('Too many items in the queue.');
        }

        $this->items[] = $item;
    }

    public function pop()
    {
        return \array_pop($this->items);
    }

    public function getCount(): int
    {
        return \count($this->items);
    }
}
