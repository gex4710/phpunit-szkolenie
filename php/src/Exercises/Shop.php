<?php

declare(strict_types=1);

namespace App\Exercises;

class Shop
{
    /**
     * @var ShopDatabase
     */
    private $database;

    public function __construct(ShopDatabase $database)
    {
        $this->database = $database;
    }

    public function addUser(ShopUser $user): bool
    {
        if ($this->database->add($user)) {
            ShopLogger::log('User added: '. \serialize($user));
            return true;
        }

        return false;
    }

    public function addProduct(ShopProduct $product): bool
    {
        if ($this->database->add($product)) {
            ShopLogger::log('Product added: '. \serialize($product));
            return true;
        }

        return false;
    }

    public function getUser(int $id): ?ShopUser
    {
        return $this->database->getUser($id);
    }

    public function getProduct(int $id): ?ShopProduct
    {
        return $this->database->getProduct($id);
    }
}