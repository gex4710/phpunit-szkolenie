<?php

declare(strict_types=1);

namespace App\Exercises;

class ShopLogger
{
    public static function log(string $message): void
    {
        echo "Logging: " . $message;
    }
}