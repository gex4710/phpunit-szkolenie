<?php

declare(strict_types=1);

namespace App\Exercises;

class ShopDatabase
{
    public const ENTITIES = [
        ShopUser::class,
        ShopProduct::class,
    ];
    /**
     * @var ShopUser[]
     */
    private $users;

    /**
     * @var ShopProduct[]
     */
    private $products;

    public function add($item): bool
    {
        foreach (self::ENTITIES as $entity) {
            if ($item instanceof $entity) {
                if($this->validateItem($item)) {
                    $this->addItem($item);
                    return true;
                }
            }
        }

        return false;
    }

    public function getUser(int $id): ?ShopUser
    {
        return $this->users[$id] ?? null;
    }

    public function getProduct(int $id): ?ShopProduct
    {
        return $this->products[$id] ?? null;
    }

    protected function validateItem($item): bool
    {
        if ($item instanceof ShopUser) {
            return \strlen($item->getName()) > 3 && \filter_var($item->getEmail(), FILTER_VALIDATE_EMAIL);
        }

        if ($item instanceof ShopProduct) {
            return $item->getName() !== '';
        }

        return false;
    }

    private function addItem($item): void
    {
        sleep(2);

        if ($item instanceof ShopUser) {
            if (!$this->users) {
                $item->setId(1);
            }else if ($item->getId() === null) {
                $item->setId(\count($this->users));
            }
            $this->users[$item->getId()] = $item;
        }

        if ($item instanceof ShopProduct) {
            if ($item->getId() === null) {
                $item->setId(\count($this->products));
            }
            $this->products[$item->getId()] = $item;
        }
    }
}