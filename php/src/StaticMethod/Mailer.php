<?php

namespace App\StaticMethod;

class Mailer
{
    public static function sendMessage($email, $message)
    {
        sleep(3);

        echo "send '$message' to '$email'";

        return true;
    }
}
