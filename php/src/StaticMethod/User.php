<?php

namespace App\StaticMethod;

class User
{
    public $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function notify($message): bool
    {
        return Mailer::sendMessage($this->email, $message);
    }
}
