<?php

namespace App\Tests\Mock;

use App\Mock\Logger;
use App\Mock\Mailer;
use App\Mock\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testReturnsFullName()
    {
        $user = new User;
        $user->first_name = "Raf";
        $user->surname = "Urb";

        $this->assertSame('Raf Urb', $user->getFullName());
    }

    public function testFullNameIsEmptyByDefault()
    {
        $user = new User;
        $this->assertSame('', $user->getFullName());
    }

    public function testNotificationIsSent()
    {
        $email = 'raf@example.com';
        $message = 'Hello World!';
        $log = \sprintf('Mail sent, message: %s | receiver: %s', $message, $email);
        $mailerMock = $this->createMock(Mailer::class);
        $mailerMock->expects($this->once())
                    ->method('sendMessage')
                    ->with($this->equalTo($email), $this->equalTo($message)) // 'raf@example.com', 'Hello'
                    ->willReturn(true);

        $loggerMock = $this->createMock(Logger::class);
        $loggerMock->expects($this->once())
            ->method('log')
            ->with($this->equalTo($log))
            ->willReturn(true);

        $user = new User($mailerMock, $loggerMock);
//        $user = new User(new Mailer(), new Logger());
        $user->email = $email;

        $this->assertTrue($user->notify($message));
    }

    /**
     * Jakiego testu(ów) brakuje?
     */
}
