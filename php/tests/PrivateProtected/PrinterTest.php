<?php

namespace App\Tests\PrivateProtected;

use App\PrivateProtected\BlackInk;
use App\PrivateProtected\ColorInk;
use App\PrivateProtected\Document;
use App\PrivateProtected\Printer;
use PHPUnit\Framework\TestCase;

class PrinterTest extends TestCase
{
    /**
     * @var BlackInk
     */
    private $blackInk;

    /**
     * @var ColorInk
     */
    private $colorInk;

    /**
     * @var Printer
     */
    private $printer;

    public function setUp(): void
    {
        $this->blackInk = new BlackInk(true);
        $this->colorInk = new ColorInk(true);
        $this->printer = new Printer($this->blackInk, $this->colorInk);
    }

    public function testPrint(): void
    {
        $text = 'Some text to be printed';
        $color = ColorInk::COLOR;

        $result = $this->printer->print($text, $color);
        $this->assertInstanceOf(Document::class, $result);
        $this->assertSame($text, $result->getText());
        $this->assertSame(ColorInk::COLOR, $result->getColor());
    }

    public function testCheckInk(): void
    {
        $blackInkMock = $this->createMock(BlackInk::class);
        $blackInkMock
            ->expects($this->once())
            ->method('isEmpty')
            ->willReturn(false);
        $printer = new Printer($blackInkMock, $this->colorInk);
        $this->assertFalse($this->callMethod($printer, 'checkInk', [BlackInk::COLOR]));

//        $this->assertFalse($this->callMethod($this->printer, 'checkInk', [BlackInk::COLOR]));
    }

    public function testCorrectInk(): void
    {
        $this->assertSame(
            $this->blackInk,
            $this->callMethod($this->printer, 'getCorrectInk', [BlackInk::COLOR])
        );
    }

    private function callMethod($object, string $method , array $parameters = [])
    {
        try {
            $className = \get_class($object);
            $reflection = new \ReflectionClass($className);
        } catch (\ReflectionException $e) {
            throw new \Exception($e->getMessage());
        }

        $method = $reflection->getMethod($method);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
