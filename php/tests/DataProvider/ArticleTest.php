<?php

namespace App\Tests\DataProvider;

use App\DataProvider\Article;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    /**
     * @var Article
     */
    private $articleClass;

    public function setUp(): void
    {
        $this->articleClass = new Article();
    }

    /**
     * @dataProvider articleDataProvider
     */
    public function testAddArticle(string $text, string $expected): void
    {
        $this->articleClass->addArticle($text);
        $this->assertSame($expected, $this->articleClass->getArticle());
    }

    public function articleDataProvider(): array
    {
        return [
            'Valid article data' => ['This is my first super article!', 'This is my first super article!'],
            'Article with HTML tags' => ['<strong>Look at my article!</strong>', 'Look at my article!'],
        ];
    }
}
