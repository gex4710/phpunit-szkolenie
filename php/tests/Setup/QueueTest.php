<?php

namespace App\Tests\Setup;

use App\Setup\TooManyItemsInQueue;
use PHPUnit\Framework\TestCase;
use App\Setup\Queue;

class QueueTest extends TestCase
{
    /**
     * @var Queue
     */
    private $queue;

    public function setUp(): void
    {
        $this->queue = new Queue;
    }

    public function testNewQueueIsEmpty(): void
    {
        $this->assertEquals(0, $this->queue->getCount());
    }
    
    public function testAnItemIsAddedToTheQueue(): void
    {
        $this->queue->push('green');
        $this->assertEquals(1, $this->queue->getCount());
    }
    
    public function testAnItemIsRemovedFromTheQueue(): void
    {
        $this->queue->push('green');
        $item = $this->queue->pop();
        $this->assertEquals(0, $this->queue->getCount());
        $this->assertSame('green', $item);
    }

    public function testMaxItemsCanBeAddedToQueue(): void
    {
        for ($i = 0; $i < Queue::MAX_QUEUE_LENGTH; $i++) {
            $this->queue->push($i);
        }

        $this->assertSame(Queue::MAX_QUEUE_LENGTH, $this->queue->getCount());
    }

    public function testQueuePushException(): void
    {
        $this->expectException(TooManyItemsInQueue::class);
        for ($i = 0; $i < Queue::MAX_QUEUE_LENGTH + 1; $i++) {
            $this->queue->push($i);
        }
    }
}