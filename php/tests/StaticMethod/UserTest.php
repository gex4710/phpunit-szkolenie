<?php

namespace App\Tests\StaticMethod;

use App\StaticMethod\Mailer;
use App\StaticMethod\User;
use Mockery;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function tearDown(): void
    {
        Mockery::close();
    }

    public function testNotify(): void
    {
        $email = 'rafa@example.com';
        $message = 'Hello!';

        $user = new User($email);

        $mock = Mockery::mock('alias:' . Mailer::class);
        $mock
            ->shouldReceive('sendMessage')
            ->once()
            ->with($email, $message)
            ->andReturn(true);

        $this->assertTrue($user->notify($message));
    }
}
