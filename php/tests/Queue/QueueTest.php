<?php

namespace App\Tests\Queue;

use App\Queue\TooManyItemsInQueue;
use PHPUnit\Framework\TestCase;
use App\Queue\Queue;

class QueueTest extends TestCase
{
    public function testNewQueueIsEmpty(): void
    {
        $queue = new Queue;
        $this->assertEquals(0, $queue->getCount());        
    }
    
    public function testAnItemIsAddedToTheQueue(): void
    {
        $queue = new Queue;
        $queue->push('green');
        $this->assertEquals(1, $queue->getCount());                        
    }
    
    public function testAnItemIsRemovedFromTheQueue(): void
    {
        $queue = new Queue;
        $queue->push('green');
        $item = $queue->pop();
        $this->assertEquals(0, $queue->getCount());
        $this->assertSame('green', $item);
    }

    public function testMaxItemsCanBeAddedToQueue(): void
    {
        $queue = new Queue();
        for ($i = 0; $i < Queue::MAX_QUEUE_LENGTH; $i++) {
            $queue->push($i);
        }

        $this->assertSame(Queue::MAX_QUEUE_LENGTH, $queue->getCount());
    }

    public function testQueuePushException(): void
    {
        $queue = new Queue();
        $this->expectException(TooManyItemsInQueue::class);
        for ($i = 0; $i < Queue::MAX_QUEUE_LENGTH + 1; $i++) {
            $queue->push($i);
        }
    }
}